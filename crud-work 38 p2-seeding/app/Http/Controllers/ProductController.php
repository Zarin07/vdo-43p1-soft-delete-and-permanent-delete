<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Database\QueryException;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class ProductController extends Controller
{
    public function index()
    {
    //   $products = Product::all();
    $products = Product::orderBy('id', 'desc')->get();
    //    dd($products);
        return view('backend.products.index', compact('products'));
    }


    public function create(){

        return view('backend.products.create');
    }

    public function store(ProductRequest $request)
    
    {
        try{
            // if($request->hasFile('image')){
            //      $image = Image::make($request->file('image'))->resize(300, 200);
            // }
            // dd($request->file('image'));

           

            //  $request->validate([
            //     'title' => 'required |min:10|unique:products,title',
            //     'price' => 'required|numeric'                           /// one way to do validation
            // ]);

             
          
            // Product::create([
            //     'title' => $request->title,
            //     'price' => $request->price
            // ]);                                 //this is one way
            

            Product::create($request->all());   //this is create method, shortcut way. in most of the case i will use it

            // $product = new Product();
            // $product->title = $request->title;
            // $product->price = $request->price;
            // $product->save();                     // this is save method to add data  in db


            //  DB::table('products')->insert([
            //      'title' => $request->title,
            //      'price' => $request->price
            //  ]);                                   // this is insert method to save data in the db


                // Session::flush('message', 'Successfully saved !');
            return redirect()->route('products.index')->withMessage('Successfully Saved !');

        } catch(QueryException $e){
             return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

       
    }

    public function show($id){
        // $product = Product::where('id', $id )->firstOrFail();  //will work same as abort(404)
        // if(!$product){
        //     abort(404);    /// id diye product na paile error page ashbe
        // }

        $product = Product::findOrFail($id);   //id diye khuje na paile abort(404 er moto dekhabe)
        
        // return view('backend.products.show', [
        //     'product' => $product                    /// evabe likha = compact kore pathano 
        // ]);                                         
        return view('backend.products.show', compact('product'));
    }

    public function edit($id){
        // dd($id);
        $product = Product::findOrFail($id);
        return view('backend.products.edit', compact('product'));
    }


    public function update(ProductRequest $request, $id){
                    //   dd($request->all());

                   

            try{ 
                 // $product = Product::findOrFail($id)->update($request->all());
                        //  $product->update($request->all());
                        
                        $product = Product::findOrFail($id);
                     $product->update([
                         'title' => $request->title,
                         'price' => $request->price,
                     ]);
        return redirect()->route('products.index')->withMessage('Successfully Updated!');

            }catch (QueryException $e){
                return redirect()->back()->withInput()->withErrors($e->getMessage());

            }
                   

                    
      
    }

    public function destroy($id){
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('products.index')->withMessage('Successfully Deleted !');
    }


    public function trash()
    { 
        $products = Product::onlyTrashed()->get();
        return view('backend.products.trash', compact('products'));
        
    }

    public function restore($id)
    {
        Product::withTrashed()
        ->where('id', $id)
        ->restore();
        return redirect()->route('products.trash')->withMessage('Restored Successsfully !');
    }


    public function delete($id)
    {
        Product::withTrashed()
        ->where('id', $id)
        ->forceDelete();

        return redirect()->route('products.trash')->withMessage('Permanently Deleted!');
    }
}

